# Depuración desde computador de Escritorio

Debe activarse modo depuración en el teléfono

En el computador de escritorio instalar adb

adb devices


Para buscar paquetes instalados en el teléfono:
 
```sh
adb shell "pm list packages -f wellbeing"
package:/data/app/com.google.android.apps.wellbeing-IiwUANr4ICwkfyifmEOh7w==/base.apk=com.google.android.apps.wellbeing
```

Para eliminar un paquete instalado en el teléfono:
```
$ adb uninstall com.google.android.apps.wellbeing 
Success
```
